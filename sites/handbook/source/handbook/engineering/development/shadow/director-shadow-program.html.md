---
layout: markdown_page
title: "Development Director Shadow Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Why have a Development Director Shadow Program?

As an [Engineering Director at GitLab](https://gitlab.com/wayne), I got massive value out of participating in our [CEO Shadow Program](/handbook/ceo/shadow/) when [I did it in July of 2020](/blog/2020/07/08/ceo-shadow-impressions-takeaways/).

Since the launch of the engineering director shadow program, I have received great feedback from the shadows from various departments (marketing, professional services, development, and customer success) that have participated.

## Benefits

### For the shadow

* Mentoring
* Learning opportunities
* Career development exploration


### For the engineering director

* Learning via reverse mentorship
* Feedback

## Criteria to be a shadow for a GitLab team member

1. I am in the Eastern US (GMT-4). The shadow must be available during some (but not all) of these working hours. Confirm this works for you before proceeding.
1. You have been with GitLab for at least one month.

## Criteria for a shadow for someone who is not a GitLab team member

1. I am in the Eastern US (GMT-4) and my work hours are 8 am to 5 pm; however this varies on a day-to-day basis. The shadow must be available during some (but not all) of these hours. Confirm this works for you before proceeding.
1. You must be willing to sign an [NDA (non-disclosure agreement)](handbook/legal/NDA/) - this is required so that we can protect confidential information.
1. You must describe why you want to participate in this program, including what you hope to learn.
1. You must be an exceptional [community open-source contributor](/community/contribute/) or a member of an [under represented group](/company/culture/inclusion/#examples-of-select-underrepresented-groups)

## Process for requesting to be a shadow

1. Check the schedule below for availability.
1. Create an MR to add yourself to the table for the weeks in which you want to shadow via an MR. To get started, you can [edit the page here](https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/sites/handbook/source/handbook/engineering/development/shadow/director-shadow-program.html.md)
1. For GitLab team members, obtain written approval from your manager that they are ok with you participating via your manager commenting in the MR that they approve, as your workload may need to be significantly reduced for the shadow program duration.  After your manager approves, assign the MR to me to review and merge.
1. For non-GitLab team members, schedule time to discuss with me via my [calendly link](https://calendly.com/whabergitlab/30min).

## How does the shadowing work?

The engineering director shadow will have a chance to work alongside engineering director [Wayne Haber](/handbook/engineering/readmes/wayne-haber/) in the following ways:

* Attend all of scheduled meetings which are not marked as `private`, unless anyone in the meeting requests privacy (discussing compensation, etc.). You will be able to attend 1-1s, group meetings, and much more.
* Review Documents, Issues, Merge Requests linked to the meetings.  Non-GitLab team members will have access to these only if they contain only public information.
* Take notes in the meeting documents in order to collect main points and outcomes. 
* Non-GitLab shadows will take any notes in a Google document that is in the GitLab Google instance and is shared with the shadow so they can edit the document. This will allow the shadow and the engineering director to collaborate on these notes and for the engineering director to review the notes for information that should not be present (such as confidential information).
* Keep track of time and provide warnings allowing meeting participants to wrap up the meeting before the meeting end time comfortably
* Provide opinions during meetings with your thoughts and/or add your thoughts in the meeting notes

### Additional shadow program considerations

* The shadow will participate remotely.
* You can be a shadow for as little as one week or as much as two weeks in a six-month period.  The weeks do not need to be sequential.
* Review the roles of those with who Wayne will be meeting in order to have more context.
* GitLab team members should feel free to ask me questions via Slack in `#wayne_shadow_program`, after a meeting concludes, via scheduling a meeting with me, or via an ad-hoc Zoom discussion.  Non-GitLab team members will do the same via a different method of async communication TBD.
* Review Wayne's [GitLab history](https://gitlab.com/wayne)
* GitLab team members should review Slack messages I have written in public channels.
* Please let Wayne know if you notice him interrupting speakers, speaking too quickly, or not pausing often enough. These are things he is working on improving.
* Feel free to introduce yourself in a meeting when you feel this is appropriate (especially when there are only a few attendees). Tell participants who you are, what your non-shadow role is, and that you are a shadow in this meeting.
* Even in meetings where you are unfamiliar with the subject matter, there is an opportunity to learn, document, and shape the evolution of GitLab's [values](/handbook/values/). 
* You may be asked for feedback on issues, merge requests, meeting notes, etc.

## Process for shadow meeting attendees

* If a meeting is public inside GitLab, shadows will attend the meeting as a regular participant
* Some meetings will discuss confidential information. In those cases, I will ask the meeting attendees if they are ok with a shadow being in the meeting (and if they are ok with the shadow having read or read/write access to the meeting notes document).  Meetings that have content that is on a [need-to-know](/handbook/communication/confidentiality-levels/) basis (such as discussions of potential acquisitions) will not include shadows.
* Only GitLab team members who are designated as insiders will be allowed access to [MNPI (Material Nonpublic information)](/handbook/business-technology/data-team/platform/safe-data/) or discussions.
* If the shadow is not a GitLab team member and a meeting is not public, Wayne will confirm with meeting attendees that having a non-GitLab team member in the meeting is acceptable.

## What this program is not

It is not a performance evaluation or a step for a future promotion.  For non-GitLab team members, it is not a direct path to a job at GitLab.

## Preparing for the program

1. Slack me in `#wayne_shadow_program` to let me know a couple of days before your first shadow
1. If you are a GitLab team-member, schedule these coffee chats a couple of days before your first shadow:
    1. With me (Wayne), especially if we have not met previously
    1. With one of the previous shadows (See list below)
1. Plan to observe and ask questions
1. Reduce your workload by at least 75% during the shadowing time period. Don't plan to do your normal amount of usual work.
1. Commit to confidentiality. Participating in the shadow program is a privilege where you may be exposed to confidential information. This is underpinned by trust in the shadows to honor the confidentiality of topics being discussed and information shared. The continuation of this program is entirely dependent on shadows past, present, and future honoring this trust placed in them.
1. For GitLab team-members, review my [calendar](https://calendar.google.com/calendar/u/0?cid=d2hhYmVyQGdpdGxhYi5jb20).
1. Review my [readme](/handbook/engineering/readmes/wayne-haber/).
1. If you are a GitLab team-member, join the slack channel `#sec-growth-datascience-people-leaders`.
1. Read GitLab's values prior to your shadow rotation, and be mindful of new and inventive ways that [CREDIT](/handbook/values/#credit) is lived out during the meetings you attend.
1. If you are a GitLab team member, confirm you have been added to the [wayne shadow program google group](https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members) so you will have access to documents that may not be open to all GitLab team members (such as 1-1 meeting notes).
1. If you are a GitLab team member and not designated an insider and would like to be included in any discussions of MNPI, request to be added to the [insider list](/handbook/legal/publiccompanyresources/).

## What to do after you participate

1. After your shadow completes, I would love to hear feedback on the program, what you learned, what you liked, what you didn't like, feedback on what I can do better, etc.
1. Consider sharing your learnings with your team and other peers via a blog, slack summary, etc.
1. If you are a GitLab team member, remove yourself from the [wayne shadow program google group](https://groups.google.com/a/gitlab.com/g/wayne-shadow-program/members) 

## Are other directors in engineering also allowing shadows?

No, not at this time.

## Blogs from previous shadows

1. [What I Learned as a Development Director](https://awkwardferny.medium.com/what-i-learned-as-an-engineering-director-shadow-at-gitlab-1a783cb564d0) - [@fjdiaz](https://gitlab.com/fjdiaz)
1. [The engineering director shadow experience at GitLab](https://about.gitlab.com/blog/2022/04/01/engineering-director-shadow/) - [@warias](https://gitlab.com/warias)


## Schedule

A given week should be considered open for shadowing unless it is listed below.

| Week of | Shadow | Department | 
| ------ | ------ | ----- |
| 9/20/2021 | [@warias](https://gitlab.com/warias) | Marketing |
| 11/15/2021 | [@mlindsay](https://gitlab.com/mlindsay) | Professional Services | 
| 12/13/2021 | [@mlindsay](https://gitlab.com/mlindsay) | Professional Services | 
| 2/21/2022 | [@oregand](https://gitlab.com/oregand) | Development |
| 2/28/2022 | [@dmishunov](https://gitlab.com/dmishunov) | Development |
| 3/28/2022 | [@rossfuhrman](https://gitlab.com/rossfuhrman) | Development |
| 4/25/2022 | [@sam.figueroa](https://gitlab.com/sam.figueroa )| Development |
| 7/18/2022 | [@bradleylee](https://gitlab.com/bradleylee) | Customer Success |
| 9/12/2022 | [@fjdiaz](https://gitlab.com/fjdiaz) | Marketing |
