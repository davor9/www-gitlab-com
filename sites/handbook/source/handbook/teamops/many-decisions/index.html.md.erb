---
layout: handbook-page-toc
title: "Decisions are the fuel for high-performance teams — TeamOps"
canonical_path: "/handbook/teamops/many-decisions/"
description: Decisions are the fuel for high-performance teams — TeamOps
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}
<%= partial("handbook/teamops/_teamops_overview.erb") %>

# Decisions are the fuel for high-performance teams

Conventional management philosophies often strive for consensus and avoid risk instead of developing a bias for action. This can result in slow decision-making. 

In TeamOps, success is correlated with decision velocity: the quantity of decisions made in a particular stretch of time (e.g. month, quarter) and the results that stem from faster progress.

Action tenets and real-world examples are below. 

## Collaboration is not consensus

TeamOps unlocks your organization's potential for making many decisions, by challenging the idea that consensus is productive. Organizations should strive to have smaller teams iterating quickly but transparently (allowing everyone to contribute), rather than a large team producing things slowly as they work toward consensus. 

Leaders and managers must moderate the desire to be involved in every decision. [Permissionless innovation](/handbook/values/#collaboration-is-not-consensus) increases a team's [bias for action](/handbook/values/#bias-for-action) and the number of decisions being made. If you choose the right [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/) and empower them to work transparently, you should not expect them to wait for a brainstorming meeting or group sign-off.

Feedback should be documented transparently. The DRI must review all feedback, but they are not required to respond to everything. This can be challenging for teams and managers, when contributions and ideas don't receive a reply. However, the experience is far superior to decisions being made in private, with limited visibility and fewer opportunities for healthy discussion.

**Here's an example:** [Implementing a replacement program for GitLab Contribute in FY23](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700)

GitLab cancelled its FY23 Contribute event due to COVID risk posed to team members from a large, global event. Many decisions were necessary in order to implement a replacement initiative. This principle enabled the DRI (Directly Responsible Individual) to ingest a lot of thoughtful feedback in a [Manager Mention Merge Request for a FY23-Q3 Visiting Grant Program](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107700). Although not every comment was replied to, everyone at the company was able to contribute feedback. Ultimately, the feedback was addressed and decisions were made in a 25-minute sync session, enabling GitLab team members to start planning their FY23-Q3 travel plans. 

## Push decisions to the lowest possible level

Decisions should be made by the person doing the work, not by their manager or their manager's manager. This enables each person to make more decisions, and frees senior leaders from the burden of making decisions where there are no blockers. 

Most times, it's better to execute on a sub-optimal decision with full conviction rather than the other way around. 

**Here's an example:** [Updating Developer Evangelism mentoring guidelines](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903)

A Senior Developer Evangelist at GitLab recognized that many coaching and mentoring sessions are shared in private 1:1 conversations. In an effort to add context and transparency to the process — thereby enabling other developer evangelists to make more decisions on their own — he [documented and merged feedback examples](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107903). The person doing the work is empowered to make the decision. In this example, the decision involved many micro decisions: to document or not; what context to add; where to document; what examples to share; how to share within the company. 

## Reduce politics

In a conventional workplace, career progression is often achieved partly by results, and partly by one's political savviness. "Playing politics" is built into the system, at times creating conflicting priorities: should you make a decision that drives results, or one that makes superiors feel good?

In a TeamOps organization, results must be rewarded, while playing politics is [explicitly counter to core values](/handbook/values/#playing-politics-is-counter-to-gitlab-values). This approach encourages more decisions, because more decisions enable more business results. Individual team members waste fewer cycles ﻿processing how a decision will be perceived politically, and there should be correlation between a reduction in politics and the quantity of decisions a team member is able to make.

**Here's an example:** [Availability over Velocity](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/30046)

In a merge request, GitLab's Chief Technology Officer proposes a change "[meant to specifically invert availability and velocity](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/30046)." In the comment thread, there are a series of healthy discussions and suggestions for changing the initial proposal. Dozens of decisions are made in just a few days, catalyzed by an environment where politics are reduced. 

Everyone at the company is able to contribute feedback, and no work occurs in a silo. Discussions are focused on _company_ interests, not _personal_ interests. Information is shared early with all parties.

## Say why, not just what

It can be tempting for a risk-intolerant organization to announce a change without much context in hopes that it will "fly under the radar" and avoid controversy. TeamOps organizations recognize that up-front transparency is a foundational element to more frequent decision-making. This requires you to not only share ***what*** the decision is, but also [***why***](/handbook/values/#say-why-not-just-what) it's being made. 

While saying "why" does not mean *justifying* every decision against other alternatives, it does require a DRI or leader to [articulate their reasoning](/handbook/values/#articulate-when-you-change-your-mind). This prevents speculation and [builds trust](/handbook/leadership/building-trust/), which is one of the traits of being a [great remote manager](/company/culture/all-remote/being-a-great-remote-manager/). Beyond these cultural benefits, saying "why" also creates institutional memory that is powerful for the future efficiency of your organization. When a related change is being made a year later, the new DRI will have access to the [well-documented](/company/culture/all-remote/handbook-first-documentation/#why-does-handbook-first-documentation-matter) reasoning behind past decisions. They're able to better understand the context, avoid duplicating work or mistakes, and make more consecutive decisions.

**Here's an example:** [Updating GitLab's recruitment privacy policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107652)

GitLab's Recruitment Privacy Policy was updated. Rather than updating the policy behind closed doors, the [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/107652)  outlines the *why*. It provides context into how the change enables cross-functional groups to work more efficiently. The explanation of *why* enables more thoughtful conversation around a potentially polarizing topic (privacy). 

## Asynchronous workflows

Asynchronous work is about more than just freeing up calendar space. In TeamOps, asynchronous workflows enable decisions to be made without a meeting. In conventional organizations, decisions are tightly linked to meetings, which severely limits how many decisions can be made. 

Instead of spending time scouring schedules and time zone differences to discuss something synchronously, shift the focus to creating clear documentation that will allow team members to contribute on their own time, and with more intentionality. This [gives agency](/handbook/values/#give-agency), reinforces a [bias for action](/handbook/values/#bias-for-action), and [bridges the knowledge gap](/company/culture/all-remote/asynchronous/#6-asynchronous-work-bridges-the-knowledge-gap), resulting in *more* total iterations. 

Establishing a thriving asynchronous culture also requires leaders to [celebrate incremental improvements](/company/culture/all-remote/asynchronous/#celebrate-incremental-improvements), encouraging their team to strive for [iteration](/handbook/values/#iteration) and [progress, not perfection](/company/culture/all-remote/asynchronous/#aim-for-progress-not-perfection). 

**Here's an example:** [Adjusting expensable entries in GitLab's expense report policy](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471)

In many organizations, altering the expense report policy would require — at minimum — one meeting. In an organization powered by TeamOps, the proposal [begins as documentation](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104471). Robert M., a senior engineer at GitLab, identified the need to reverse a policy change. He documented his reasoning in a merge request, and notified the Procurement team and other stakeholders. This allowed feedback to be gathered and the appropriate owners of the policy to consider the changes at a time that worked best for them. 

Scaled across an organization, this meeting-free approach to making decisions enables more decisions to be made. This approach allows a more diverse array of perspectives to influence the decision, as there was no requirement to align 13 individuals to a single time slot on a given day for a synchronous meeting. 

## Boring solutions 

It is tempting to seek out the most cutting-edge, complex, or interesting solution to solve a problem. Instead, TeamOps encourages choosing ["boring" or simple solutions](/handbook/values/#boring-solutions). By taking every opportunity to reduce complexity in the organization, you're able to increase the speed and frequency of innovation.

One boring solution you may often see: researching what other successful organizations are doing and adopting their methods, rather than reinventing a process.

Embracing boring solutions and shipping the [minimum viable change (MVC)](/handbook/values/#move-fast-by-shipping-the-minimal-viable-change) also means [accepting mistakes](/handbook/values/#accept-mistakes) if that solution doesn't work, and moving on to the next [iteration](/handbook/values/#iteration). Because changes are small, mistakes are far less costly. This encourages more decision-making in a shorter span of time. 

**Here's an example:** [Solving a GitLab attribution problem by improving git commit message](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130)

Nick V., a director at GitLab, [proposed a boring solution](https://gitlab.com/gitlab-com/quality/contributor-success/-/issues/130) to use existing functionality in new ways. By modifying a few lines in an automated message, he was able to solve an organization-wide problem with attribution. 

In a TeamOps organization, boring solutions are celebrated *because of* their simplicity. There is always a possibility to add more polish or functionality, if it's needed in the future. The initial boring solution enables more decisions to be made, more quickly.

## Only healthy constraints 

Organizational growth does not have to result in stagnation. Leaders and managers must resist the tendency to put new processes and approvals are put in place because it's how other "mature" companies operate, slowing the pace of decision-making and innovation (e.g. [Slime Molds](https://komoroske.com/slime-mold/)).

TeamOps requires an awareness of the constraints that develop as a company grows, and an active approach toward removing them. TeamOps leaders must know the difference between [healthy (useful) constraints and unhealthy ones](/handbook/only-healthy-constraints/), and create a system for unblocking and reducing inefficiencies in the company. Ideally, a TeamOps organization can continue to operate with the agility of a startup while realizing the efficiencies of a scaling company.

**Here's an example:** [GitLab's list of tactics to resist unhealthy constraints](/handbook/only-healthy-constraints/#resisting-unhealthy-constraints)

In GitLab's S-1 filing, CEO Sid Sijbrandij documented some of the ways that GitLab plans to "remain a startup" and avoid the stagnation experienced by most early stage companies as they mature. These tactics are also [documented in the company handbook](/handbook/only-healthy-constraints/#resisting-unhealthy-constraints) as a living, iterating list of ideas and recommendations that anyone in the company can use to resist unhealthy constraints.

---

Return to the [TeamOps](/handbook/teamops/) homepage. 
