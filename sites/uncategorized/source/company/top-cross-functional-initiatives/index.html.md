---
layout: markdown_page
title: "Top Cross-Functional Initiatives"
description: "Top cross-functional initiatives are key to GitLab's success in the fiscal year and beyond"
canonical_path: "/company/team/structure/working-groups/top-cross-functional-initiatives/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are Top Cross-Functional Initiatives?

Top Cross-Functional Initiatives are [Working Groups](https://about.gitlab.com/company/team/structure/working-groups/) that are key to GitLab's success in the fiscal year and beyond. While there are other important business initiatives and priorities that exist within functions or require engagement across the business, we elevate these initiatives to address cross-functional dependencies, align on goals, and ensure ongoing reporting and monitoring. 

These projects are the most important _cross-functional_ ones, not necessarily the most important projects and initiatives. Other projects may be of great importance throughout the business, for example meeting recruiting targets or category leadership, but these are workstreams led by individual functions and central to their key business responsibilities. When alignment or ongoing reporting is required for an initiative clearly owned by a single function, the responsible E-Group lead should ensure that Key Reviews, E-Group Weeklies, Group Conversations and other existing forums are used for updates. 

Please note that a limited set of team members currently have access to the linked Epics. We are exploring ways to make more information available to all team members and the wider-community while keeping sensitive information contained to a smaller group.

As Top Cross-Functional Initiatives are Working Groups, the same [process](/company/team/structure/working-groups/#process) should be followed to ensure they have goals and exit criteria.

### Current Top Cross-Functional Initiatives

This is the list of our active Top Cross-Functional Initiatives. 

| Initiative | DRI / Sponsor | Description | Links |
| ------ | ------ |------ | ------ |
| Usage Reporting | @jdbeaumont / @mmcb   | Ensure customer product usage and utilization metrics are surfaced to the right teams at the right time to drive improvements in customer adoption and flag high potential user growth/tier-upgrade opportunities. | [Operational Data Vision](https://about.gitlab.com/handbook/customer-success/product-usage-data/), [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/1) |
| Fulfillment Efficiency | @ofernandez2 / @sytses  | Rollout new Quote Studio using new Zuora Orders API and Ramps functionality. Move CustomersDot to use Zuora Orders APIs. Align to Zuora Billing Account as a Customer Account. SSO / combine CDot and Gitlab user databases. Link ArrowSphere to CustomersDot. | [FY23 Roadmap](https://docs.google.com/presentation/d/1eTH09QZqnazJ4jh-jVGTEYEFA3m6R2jwWuhY5_EtPL4/edit#slide=id.g123a13deda8_0_405) |
| FedRAMP | @JohnathanHunt / @akramer | Achieve FedRAMP Moderate Certification. | [Working group page](/company/team/structure/working-groups/fedramp-execution/), [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/3) |
| Project Horse | @marin / @akramer  | [Internally confidential](https://about.gitlab.com/handbook/communication/confidentiality-levels/#internal) | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/4)  |
| SaaS Reliability | @sloyd / @akramer  | Achieve enterprise grade security and reliability for our SaaS. | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/5) |
| SaaS Free User Efficiency | @ipedowitz / @akramer  | Adjust free program offerings to increase conversion rate and reduce sales and marketing cost while still giving back to students, startups, educational institutions, open source projects, GitLab contributors, and nonprofits. | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/free-saas-user-efficiency/-/epics/1) |
| User Engagement | @johncoghlan / @akramer  | Create a developer brand through generating awareness among various communities that become advocates of GitLab and influence purchase decisions. | [Epic](https://gitlab.com/groups/gitlab-com/-/epics/1794) |
| Project Matterhorn | @keithchung / @david   | [Limited access](https://about.gitlab.com/handbook/communication/confidentiality-levels/#limited-access) | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/7) |
| First Order | @christinelee / @akramer   | Grow our new logo and SAO counts across all segments to meet the business plan and set ourselves up for long-term growth. |[Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/9)  |
| AWS/GCP Partnerships | @nbadiey @mmcb   | Expand GitLab's access to AWS/GCP LAM via cloud-first sales plays, new products, and lower friction buying. | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/11)  |
| E-Commerce | @ronell / @mmcb | Create world-class buyer experience and significant self-serve customer base / ARR. | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/12)  |

### Managing, Adding and Removing Top Cross-Functional Initiatives

We assess progress against each initiative in related [Key Reviews](/handbook/key-review/) and [Group Conversations](/handbook/group-conversations/). We also have a dedicated quarterly meeting in which initiative DRIs, sponsors, E-Group, and other key participants review initiative health, risks, and blockers. 

Top Cross-Functional Initiatives are not intended to last forever. Initiatives can be retired for a few reasons, including:

1. The team has met its exit criteria. There is agreement that the team has achieved its goals. 
1. Work is not truly cross-functional. While of importance to all of GitLab, the initiative is at a point where it is best managed within a single function. The E-Group Sponsor will be responsible for ensuring ongoing awareness and progress. 
1. Other initiatives take higher priority. While there may be additional work to be done, we maintain a limited set of Top Cross-Functional Initiatives. E-Group may choose to retire an initiative from the list if other initiatives are of higher importance to the success of the business.

Top Cross-Functional Initiatives are discussed in the annual planning process. In the Q3 [E-Group Offsite](/company/offsite/), E-Group will review the list of current Top Cross-Functional Initiatives. It will also discuss priorities for the coming year. In Q4 of the fiscal year, E-Group will designate some initiatives for retirement. While some initiatives may remain on the list, their scope may be revisited as part of this process.

Any addition or retirement of Top Cross-Functional Initiatives should be shared with E-Group in advance of retirement. It is important that there is room for alignment and discussion in advance of removal. This can happen in a designated forum as part of the annual planning process or through a discussion topic in an [E-Group Weekly](https://about.gitlab.com/handbook/e-group-weekly/). After the E-Group Sponsor and CEO have agreed to retire the initiative, and E-Group has had a forum for any feedback; the E-Group Sponsor is responsible for ensuring that the initiative is appropriately closed out and any internal and/or external communication is thoughtfully managed. 

When an initiative is retired, it should be moved from the [current initiative list](#current-top-cross-functional-initiatives) to the [retired initiative list](#retired-top-cross-functional-initiatives).

### Cross-Functional Initiative DRIs

Each of our top cross-functional initiatives has an Executive Sponsor and a [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/). DRIs are the folks ultimately held accountable for the success (or failure) of the project. They are the linchpins as these initiatives require deep cross-functional alignment and coordination. DRIs are also responsible for performance tracking and reporting. This includes providing updates in advance of relevant [Key Reviews](https://about.gitlab.com/handbook/key-review). Updates should map to the Executive Sponsor who is aligned with the initiative. For example, category leadership maps to Sales and Marketing as the Exec Sponsor is the CMO. The DRI is responsible for the following deliverables in advance of Key Reviews:
   1. The initiative overview epic
   1. Milestone issue status 
   1. Two initiative slides using the [Key Review meeting template](https://docs.google.com/presentation/d/1cdo138YJcFl_x9-vaybRJgcO_eSiLnOGw9b-JZidX3A/edit?usp=sharing) 

### Information Access

We are tracking in epics and issues in a separate namespace to comply with [SAFE guidelines](/handbook/legal/safe-framework/). If team members want access to a specific initiative, they should reach out to the appropriate DRI. Links to each initiative and other details are captured in the [chart above](/company/team/structure/working-groups/#top-cross-functional-initiatives). 

## Retired Top Cross-Functional Initiatives

This is a a list of top cross-functional initiatives that have been retired. 

| Initiative | DRI / Sponsor | Description | Links | Start / Year |
| ------ | ------ |------ | ------ | ------ |
|Category Leadership | @melsmo / @akramer   | Become the overall leader of The DevOps Platform through defining and owning the category. **This is still important to GitLab, but it will be managed by the Marketing Team, not as a top cross-functional initiative** | [Working group page](/company/team/structure/working-groups/category-leadership/), [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/10) | FY23-Q1 - FY23-Q2 |
| Cloud Licensing | @ofernandez2 / @david  | Provide automated licensing functionality to customers to improve their experience while reducing the chances of sales and support escalations. | [Epic](https://gitlab.com/groups/gitlab-com-top-initiatives/-/epics/2) | FY23-Q1 - FY23-Q3 |
